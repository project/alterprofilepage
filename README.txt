About this module ("Alter profile page")
----------------------------------------
It's really quite simple.  When enabled, this module will change the appearance of user profile page (at http://www.example.com/user/user_name_goes_here).

What gets altered
-----------------
- "Username's profile" appears on the user's profile page instead of the word "History"
- Below that, rather than displaying "Member for 8 minutes 34 seconds" that wording will be changed to "Member since September 2009"
- Additionally, between "Username's profile" and the field labeled "Member since" another field will show up which is labeled "Last login" and the value IT displays is (you guessed it!) the user's last login date.